name = "Rafting"
map = [[

   ph
   gg
  ggg                                               
wggggg                                              
gggggg                                              
gggggg                      b                       
ggggggg          ga        ggggggggg  ggg         a 
gggggggwwwwwwwwwwggggwwwwwwggggggggg    g        ggg
ggggggggggggggggggggggggggggggggggggggggg       gggg
gggggggggggggggggggggggggggggggggggggggggwwwwwwggggg
gggggggggggggggggggggggggggggggggggggggggggggggggggg
]]

intro = nil

level = Level:new(name, map, intro)

return level
