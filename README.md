# Shepherd's Island

A turn-based puzzle game on a grid, with stacking objects and water flow mechanics.

Built in 72 hours for Ludum Dare 50 (Theme: Delay the Inevitable).

See https://ldjam.com/events/ludum-dare/50/shepherds-island for more info about the game.

## Music:

- "Take a Chance" Kevin MacLeod (incompetech.com)
- "Bleeping Demo" Kevin MacLeod (incompetech.com)

Licensed under Creative Commons: By Attribution 4.0 License
http://creativecommons.org/licenses/by/4.0/

## Sound effects:

- https://freesound.org/people/klankbeeld/sounds/515878/
- https://freesound.org/people/Erdie/sounds/34538/
- https://freesound.org/people/deleted_user_4401185/sounds/245103/
- https://freesound.org/people/bolkmar/sounds/539177/
- https://freesound.org/people/k06a/sounds/231751/
- https://freesound.org/people/SergioFlorez/sounds/387304/
- https://freesound.org/people/swuing/sounds/38873/
- https://freesound.org/people/MattRuthSound/sounds/562043/

## Fonts:

The fonts are from the DigitalDisco family.
